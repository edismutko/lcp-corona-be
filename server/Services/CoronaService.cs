﻿using server.Interfaces;
using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace server.Services
{
    public class CoronaService:ICoronaService
    {
        private readonly ICoronaAccessor _coronaAccessor;
        public CoronaService(ICoronaAccessor coronaAccessor)
        {
            _coronaAccessor = coronaAccessor;
        }                

        public Task<IEnumerable<CoronaStatusPerDay>> GetCoronaStatus(string countryCode, string startDate, string endDate)
        {
            return _coronaAccessor.GetCoronaStatus(countryCode, startDate, endDate);
        }
    }
}
