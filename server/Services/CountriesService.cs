﻿using server.Accessors;
using server.Interfaces;
using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.Services
{
    public class CountriesService : ICountriesService
    {
        private readonly ICountriesAccessor _countriesAccessor;
        private readonly CountriesMockAccessor _mockCountriesAccessor;
        public CountriesService(ICountriesAccessor countriesAccessor, CountriesMockAccessor mockCountriesAccessor)
        {
            _countriesAccessor = countriesAccessor;
            _mockCountriesAccessor = mockCountriesAccessor;
        }
        public async Task<IEnumerable<Country>> GetCountries()
        {
            //return _mockCountriesAccessor.GetCountries();

            try
            {
                return await  _countriesAccessor.GetCountries();
            }
            catch(Exception)
            {
                return await _mockCountriesAccessor.GetCountries();
            }

        }
    }
}
