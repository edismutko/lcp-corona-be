﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace server.Models
{
    public class CoronaStatusPerDay
    {
        [JsonProperty(PropertyName = "new_deaths")]
        public int Deaths { get; set; }
        [JsonProperty(PropertyName = "new_infections")]
        public int Infections { get; set; }
        [JsonProperty(PropertyName = "new_recovered")]
        public int Recovered { get; set; }
        [JsonProperty(PropertyName = "last_updated")]
        public DateTime Date { get; set; }
        [JsonIgnore]
        public double growthPercentages { get; set; }
        [JsonIgnore]
        public bool IsPresentLess { get; set; }//getting less daily infections or more
    }
}
