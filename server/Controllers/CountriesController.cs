﻿using Microsoft.AspNetCore.Mvc;
using server.Interfaces;
using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace server.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        readonly private ICountriesEngine _countriesEngine;
        public CountriesController(ICountriesEngine countriesEngine)
        {
            _countriesEngine = countriesEngine;
        }

        // GET: api/<CountriesController>
        [HttpGet]
        public Task<IEnumerable<Country>> Get()
        {
            return _countriesEngine.GetCountries();
        }
      
    }
}
