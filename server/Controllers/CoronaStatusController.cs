﻿using Microsoft.AspNetCore.Mvc;
using server.Interfaces;
using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace server.Controllers
{    
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CoronaStatusController : ControllerBase
    {
        private readonly ICoronaEngine _coronaEngine;
        public CoronaStatusController(ICoronaEngine coronaEngine)
        {
            _coronaEngine = coronaEngine;
        }

       
        [HttpGet("{countryCode}")]
        public Task<CoronaStatusPerDay> Get(string countryCode)
        {
           return _coronaEngine.GetCoronaStatus(countryCode);
        }

        
    }
}
