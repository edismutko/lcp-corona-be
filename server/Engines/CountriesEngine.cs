﻿using server.Interfaces;
using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.Engines
{
    public class CountriesEngine : ICountriesEngine
    {
        readonly private ICountriesService _countriesService;

        public CountriesEngine(ICountriesService countriesService)
        {
            _countriesService = countriesService;
        }
        public async Task<IEnumerable<Country>> GetCountries()
        {
            return await _countriesService.GetCountries();
        }

        
    }
}
