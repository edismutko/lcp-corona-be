﻿using server.Interfaces;
using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.Engines
{
    public class CoronaEngine : ICoronaEngine
    {
        private readonly ICoronaService _coronaService;

        public CoronaEngine(ICoronaService coronaService)
        {
            _coronaService = coronaService;
        }

        public async Task<CoronaStatusPerDay> GetCoronaStatus(string countryCode)
        {
            var startDate = DateTime.Now.AddDays(-14).ToString("yyyy-MM-dd");
            var endDate = DateTime.Now.ToString("yyyy-MM-dd");
          
            var lastTwoWeeksStatus = await _coronaService.GetCoronaStatus(countryCode, startDate, endDate);

            var lastTwoWeeksStatusList = lastTwoWeeksStatus?.ToList();
            var count = lastTwoWeeksStatusList != null ? lastTwoWeeksStatusList.Count : 0;
            var currentStatus = count > 0 ? lastTwoWeeksStatusList[0] : null;
            var prevInfections = count > 1 ? lastTwoWeeksStatusList[1].Infections : 0;
            var prevTwoWeeksInfections = count > 0 ? lastTwoWeeksStatusList[count - 1].Infections : 0;

            if (currentStatus != null)
            {
                currentStatus.growthPercentages = currentStatus.Infections > prevInfections?(currentStatus.Infections/((double)prevInfections/100))-100:0;
                currentStatus.IsPresentLess = prevTwoWeeksInfections < currentStatus?.Infections;
            }

            return currentStatus;
        }
      
    }
}
