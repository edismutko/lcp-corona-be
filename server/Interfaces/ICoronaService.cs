﻿using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.Interfaces
{
    public interface ICoronaService
    {
        Task<IEnumerable<CoronaStatusPerDay>> GetCoronaStatus(string countryCode, string startDate, string endDate);

    }
}
