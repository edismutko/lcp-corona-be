﻿using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.Interfaces
{
    public interface ICountriesAccessor
    {
        Task<IEnumerable<Country>> GetCountries();
    }
}
