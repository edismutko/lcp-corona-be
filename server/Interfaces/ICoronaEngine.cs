﻿using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.Interfaces
{
    public interface ICoronaEngine
    {
        Task<CoronaStatusPerDay> GetCoronaStatus(string countryCode);
    }
}
