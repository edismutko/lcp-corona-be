﻿using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.Interfaces
{
    public interface ICountriesService
    {
        /// <summary>
        /// Returns a list of countries
        /// </summary>
        /// <returns>DTO object of type Country</returns>
        Task<IEnumerable<Country>> GetCountries();
    }
}
