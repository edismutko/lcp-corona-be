﻿using Newtonsoft.Json.Linq;
using server.Interfaces;
using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace server.Accessors
{
    public class CountriesAPIAccessor : ICountriesAccessor
    {
        private readonly string[] _countriesCodes = { "AT", "BE", "CA", "FR", "DE", "IL", "IT" };
        private const string _baseURL = "http://www.geognos.com/api/en/countries";

        public async Task<IEnumerable<Country>> GetCountries()
        {
            var countries = new List<Country>();

            foreach (var countryCode in _countriesCodes)
            {
                var url = GetUrl("info", "json", countryCode);

                using var client = new HttpClient();
                using var res = await client.GetAsync(url);
                using var content = res.Content;

                var data = await content.ReadAsStringAsync();
                var apiResult = JObject.Parse(data).ToObject<APIResult>();

                if (apiResult.StatusMsg.ToUpper() == "OK")
                {
                    countries.Add(new Country()
                    {
                        Code = apiResult.Results.CountryCodes.iso2,
                        Name = apiResult.Results.Name,
                        Capital = apiResult.Results.Capital.Name,
                        Flag = GetUrl("flag", "png", countryCode)
                    });
                }
            }
            return countries;
        }

        private string GetUrl(string serviceName, string outputFormat, string countryCode)
        {
            return string.Format("{0}/{1}/{2}.{3}", _baseURL, serviceName, countryCode, outputFormat);
        }

        class APIResult
        {
            public string StatusMsg { get; set; }
            public Country Results { get; set; }

            public class Country
            {
                public string Name { get; set; }
                public Capital Capital { get; set; }
                public CountryCodes CountryCodes { get; set; }

            }
            public class Capital
            {
                public string Name { get; set; }
            }
            public class CountryCodes
            {
                public string iso2 { get; set; }
            }
        }
    }

}

