﻿using Newtonsoft.Json.Linq;
using server.Interfaces;
using server.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace server.Accessors
{
    public class CoronaAPIAccessor : ICoronaAccessor
    {

        public async Task<IEnumerable<CoronaStatusPerDay>> GetCoronaStatus(string countryCode, string startDate, string endDate)
        {
            var baseURL = string.Format("http://api.coronatracker.com/v5/analytics/newcases/country?countryCode={0}&startDate={1}&endDate={2}", countryCode, startDate, endDate);

            using var client = new HttpClient();
            using var res = await client.GetAsync(baseURL);
            using var content = res.Content;
            
            var data = await content.ReadAsStringAsync();                 
            return JArray.Parse(data).ToObject<IEnumerable<CoronaStatusPerDay>>();
        }
    }
}
