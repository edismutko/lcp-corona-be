﻿using server.Models;
using server.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server.Accessors
{
    public class CountriesMockAccessor : ICountriesAccessor
    {
        readonly private IEnumerable<Country> _countries;

        public CountriesMockAccessor()
        {
            _countries = new List<Country>()
            {
                new Country() {Code="AT",Name="Austria", Capital="Vienna",Flag="http://www.geognos.com/api/en/countries/flag/AT.png"},
                new Country() {Code="BE",Name="Belgium", Capital="Brussles",Flag="http://www.geognos.com/api/en/countries/flag/BE.png"},
                new Country() {Code="CA",Name="Canada", Capital="Ottawa",Flag="http://www.geognos.com/api/en/countries/flag/CA.png"},
                new Country() {Code="FR",Name="France", Capital="Paris",Flag="http://www.geognos.com/api/en/countries/flag/FR.png"},
                new Country() {Code="DE",Name="Germany", Capital="Berlin",Flag="http://www.geognos.com/api/en/countries/flag/DE.png"},
                new Country() {Code="IL",Name="Israel", Capital="Jerusalem",Flag="http://www.geognos.com/api/en/countries/flag/IL.png"},
                new Country() {Code="IT",Name="Italy", Capital="Rome",Flag="http://www.geognos.com/api/en/countries/flag/IT.png"}
            };
        }
       
        public Task<IEnumerable<Country>> GetCountries() => Task.FromResult(_countries);
        //public Task<IEnumerable<Country>> GetCountries() => (new Task<IEnumerable<Country>>(() => _countries));
        
    }
}
